package com.thinkpigit.useraccountapp;

/**
 * Created by bleac on 01/12/2016.
 */

public class Constants {

    // SharedPreferences keys
    public static final String USERNAME = "username";
    public static final String PASSWORD = "password";
    public static final String IS_LOGGED_IN = "isLoggedIn";
    public static final String USER_ID = "userId";
    public static final String TOKEN = "token";

    // API paths
    public static final String BASE_URL = "http://www.myuseraccountapi.com/";
    public static final String NEW_SESSION = "sessions/new";
    public static final String GET_USER_PROFILE = "users/{userid}";
    public static final String POST_USER_AVATAR = "users/{userid}/avatar";

    // Test data
    public static final String TEST_USER_ID = "TestUser";
    public static final String TEST_TOKEN = "ABC";

}
