package com.thinkpigit.useraccountapp.connectionservices;

import android.support.v4.util.LruCache;
import android.util.Log;

import com.thinkpigit.useraccountapp.Constants;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by bleac on 01/12/2016.
 */

public class ConnectionService {

    private static Retrofit retrofit = null;
    private static OkHttpClient okHttpClient;
    private static final String TAG = "ConnectionService";
    private static LruCache<Class<?>, Observable<?>> apiObservables;

    public static UserAccountApi getConnectionService() {

        Log.d(TAG, "GetConnectionService starting");
        okHttpClient = buildClient();
        if (retrofit == null) {
            apiObservables = new LruCache<>(6);
            retrofit = new Retrofit.Builder()
                    .baseUrl(Constants.BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                    .client(okHttpClient)
                    .build();
        }
        Log.d(TAG, "GetConnectionService ending");

        return retrofit.create(UserAccountApi.class);
    }

    public static OkHttpClient buildClient() {
        Log.d(TAG, "BuildClient starting");
        OkHttpClient.Builder builder = new OkHttpClient.Builder();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        builder.addInterceptor(interceptor);
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Response response = chain.proceed(chain.request());
                // Do anything with response here
                //if we ant to grab a specific cookie or something..
                return response;
            }
        });
        builder.addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                //this is where we will add whatever we want to our request headers.
                Request request = chain.request().newBuilder().addHeader("Accept", "application/json").build();
                return chain.proceed(request);
            }

        });
        Log.d(TAG, "BuildClient ending");

        return builder.build();

    }


    public void clearCache() {
        apiObservables.evictAll();
    }


    /**
     * Method to either return a cached observable or prepare a new one.
     *
     * @param unPreparedObservable
     * @param clazz
     * @param cacheObservable
     * @param useCache
     * @return Observable ready to be subscribed to
     */
    public static Observable<?> getPreparedObservable(Observable<?> unPreparedObservable, Class<?> clazz, boolean cacheObservable, boolean useCache) {

        Log.d(TAG, "getPreparedObservableStarting");

        Observable<?> preparedObservable = null;

        if (useCache) {
            preparedObservable = apiObservables.get(clazz);
        }

        if (preparedObservable != null) {
            Log.d(TAG, "Observable found in cache");
            return preparedObservable;
        }


        preparedObservable = unPreparedObservable.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread());

        if (cacheObservable) {
            preparedObservable = preparedObservable.cache();
            apiObservables.put(clazz, preparedObservable);
        }


        return preparedObservable;
    }

}
