package com.thinkpigit.useraccountapp.connectionservices;

import com.github.pwittchen.reactivenetwork.library.ReactiveNetwork;

import rx.Observer;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

/**
 * Created by bleac on 01/12/2016.
 */

public class ConnectivityChecker {

    public void checkInternetConnectivity(final ConnectivityCheckListener listener) {
        ReactiveNetwork.observeInternetConnectivity()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<Boolean>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(Boolean aBoolean) {
                        listener.onConnectivityChecked(aBoolean);
                    }
                });
    }


    public interface ConnectivityCheckListener {
        void onConnectivityChecked(boolean isConnectedToInternet);
    }
}
