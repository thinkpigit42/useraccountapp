package com.thinkpigit.useraccountapp.connectionservices;

import com.thinkpigit.useraccountapp.Constants;
import com.thinkpigit.useraccountapp.loginactivity.LoginActivityPresenter;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;
import rx.Observable;

/**
 * Created by bleac on 01/12/2016.
 */

public interface UserAccountApi {

    @FormUrlEncoded
    @POST(Constants.NEW_SESSION)
    Observable<LoginActivityPresenter.CredentialsResponse> requestNewSession(@Field("email") String email,
                                                                             @Field("password") String password);

}
