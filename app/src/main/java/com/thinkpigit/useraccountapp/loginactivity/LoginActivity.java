package com.thinkpigit.useraccountapp.loginactivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.thinkpigit.useraccountapp.BaseActivity;
import com.thinkpigit.useraccountapp.R;
import com.thinkpigit.useraccountapp.userdetailsactivity.UserDetailsActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LoginActivity extends BaseActivity implements LoginActivityContract.ViewContract {

    @BindView(R.id.username_etxt)
    EditText usernameEtxt;
    @BindView(R.id.password_etxt)
    EditText passwordEtxt;

    ProgressDialog progressDialog;

    LoginActivityContract.PresenterContract presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initialiseUI();
        initialisePresenter();
        presenter.start();
    }

    private void initialiseUI(){
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);
    }

    private void initialisePresenter(){
        new LoginActivityPresenter(this, this);
    }

    @Override
    public void setPresenter(LoginActivityContract.PresenterContract presenter) {
        this.presenter = presenter;
    }

    public void login(View view){
        String username = usernameEtxt.getText().toString();
        String password = passwordEtxt.getText().toString();
        presenter.beginLogin(username, password);
    }

    public void startProgressDialog(String message){
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.setMessage(message);
            progressDialog.setCancelable(false);
            progressDialog.show();
        }
    }

    public void stopProgressDialog(){
        if (progressDialog != null){
            progressDialog.dismiss();
            progressDialog = null;
        }
    }

    public void displayToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void launchUserDetailsActivity(){
        Intent intent = new Intent(this, UserDetailsActivity.class);
        startActivity(intent);
    }

}
