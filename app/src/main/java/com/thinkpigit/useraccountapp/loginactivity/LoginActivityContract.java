package com.thinkpigit.useraccountapp.loginactivity;

import com.thinkpigit.useraccountapp.mvp.BasePresenterContract;
import com.thinkpigit.useraccountapp.mvp.BaseViewContract;

/**
 * Created by bleac on 01/12/2016.
 */

public interface LoginActivityContract {

    interface PresenterContract extends BasePresenterContract {
        void beginLogin(String username, String password);
    }

    interface ViewContract extends BaseViewContract<PresenterContract> {
        void startProgressDialog(String message);
        void stopProgressDialog();
        void displayToast(String message);
        void launchUserDetailsActivity();
    }
}
