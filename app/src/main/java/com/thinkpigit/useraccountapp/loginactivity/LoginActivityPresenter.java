package com.thinkpigit.useraccountapp.loginactivity;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.thinkpigit.useraccountapp.Constants;
import com.thinkpigit.useraccountapp.connectionservices.ConnectionService;
import com.thinkpigit.useraccountapp.connectionservices.ConnectivityChecker;
import com.thinkpigit.useraccountapp.connectionservices.UserAccountApi;

import rx.Observable;
import rx.Observer;

/**
 * Created by bleac on 01/12/2016.
 */

public class LoginActivityPresenter implements LoginActivityContract.PresenterContract,
        ConnectivityChecker.ConnectivityCheckListener {

    LoginActivityContract.ViewContract view;
    UserAccountApi api;
    Context context;

    private String username;
    private String password;

    public LoginActivityPresenter(Context context, LoginActivityContract.ViewContract view){
        this.view = view;
        this.context = context;
        view.setPresenter(this);
    }

    @Override
    public void start() {
        connectToApi();
    }

    private void connectToApi() {
        api = ConnectionService.getConnectionService();
    }

    @Override
    public void beginLogin(String username, String password) {
        this.username = username;
        this.password = password;
        if (!username.equals("") && !password.equals("")) {
            view.startProgressDialog("Logging in");
            new ConnectivityChecker().checkInternetConnectivity(this);
        } else {
            view.displayToast("You must enter a username and password");
        }
    }

    @Override
    public void onConnectivityChecked(boolean isConnectedToInternet) {
        if (isConnectedToInternet){
            verifyCredentials();
        } else {
            view.stopProgressDialog();
            view.displayToast("Unable to login - connection unavailable");
        }
    }

    private void verifyCredentials() {

        Observable<CredentialsResponse> userValidationObservable = (Observable<CredentialsResponse>)
                ConnectionService.getPreparedObservable(api.requestNewSession(username, password),
                        CredentialsResponse.class, false, false);

        userValidationObservable.subscribe(new Observer<CredentialsResponse>() {
            @Override
            public void onCompleted() {

            }

            @Override
            public void onError(Throwable e) {
                onCredentialsError();
            }

            @Override
            public void onNext(CredentialsResponse credentialsResponse) {
                readCredentialsResponse(credentialsResponse);
            }
        });
    }

    private void readCredentialsResponse(CredentialsResponse credentialsResponse) {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(Constants.USERNAME, username);
        editor.putString(Constants.PASSWORD, password);
        editor.putString(Constants.USER_ID, credentialsResponse.userId);
        editor.putString(Constants.TOKEN, credentialsResponse.token);
        editor.putBoolean(Constants.IS_LOGGED_IN, true);
        editor.apply();
        login();
    }

    private void onCredentialsError() {
        view.stopProgressDialog();
        view.displayToast("Unable to get authorisation - logging in for testing purposes only");

        // For testing purposes, log in anyway
        loginForTesting();
    }

    private void loginForTesting() {
        CredentialsResponse credentialsResponse = new CredentialsResponse();
        credentialsResponse.userId = Constants.TEST_USER_ID;
        credentialsResponse.token = Constants.TEST_TOKEN;
        readCredentialsResponse(credentialsResponse);
    }

    private void login(){
        view.launchUserDetailsActivity();
    }

    public static class CredentialsResponse {
        public String userId;
        public String token;
    }
}
