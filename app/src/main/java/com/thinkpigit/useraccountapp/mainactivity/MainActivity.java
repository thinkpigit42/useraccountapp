package com.thinkpigit.useraccountapp.mainactivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.thinkpigit.useraccountapp.BaseActivity;
import com.thinkpigit.useraccountapp.Constants;
import com.thinkpigit.useraccountapp.loginactivity.LoginActivity;
import com.thinkpigit.useraccountapp.userdetailsactivity.UserDetailsActivity;


public class MainActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        determineLoginStatus();
    }

    // If user is not already logged in, launch LoginActivity.  If they are already logged in, launch UserDetailsActivity.
    private void determineLoginStatus(){
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        boolean isLoggedIn = preferences.getBoolean(Constants.IS_LOGGED_IN, false);
        Intent intent;
        if (isLoggedIn){
            intent = new Intent(this, UserDetailsActivity.class);
        } else {
            intent = new Intent(this, LoginActivity.class);
        }
        startActivity(intent);
    }
}
