package com.thinkpigit.useraccountapp.mvp;

/**
 * Created by bleac on 01/12/2016.
 */

public interface BaseViewContract<T> {

    void setPresenter(T presenter);

}
