package com.thinkpigit.useraccountapp.userdetailsactivity;

import android.os.Bundle;

import com.thinkpigit.useraccountapp.BaseActivity;
import com.thinkpigit.useraccountapp.R;

public class UserDetailsActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.user_details_activity);
    }
}
